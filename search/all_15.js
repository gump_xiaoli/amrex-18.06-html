var searchData=
[
  ['uncache',['uncache',['../db/de8/classamrex_1_1_agg_stencil.html#a1db7db53a67c68e8b4161ab18b880dc7',1,'amrex::AggStencil']]],
  ['unconstrained',['UNCONSTRAINED',['../dd/dde/class_constrained_l_s.html#a96327cdbef77a743c8b0554ca6a4ea7cae2f6373e7f2c294bab750551caa32f8a',1,'ConstrainedLS']]],
  ['unconverged',['UNCONVERGED',['../dd/dde/class_constrained_l_s.html#a6e412b9d41a6cfeb6b166abf4995491fabf84ff2ae9518fad36bb7c696ffdb475',1,'ConstrainedLS']]],
  ['undef',['undef',['../d9/de4/classamrex_1_1_coord_sys.html#afb633344fd17c5565ce2fff188806ebaa2debcd8f37986ae0e3c488f583234ccc',1,'amrex::CoordSys']]],
  ['undefined',['UNDEFINED',['../dd/d64/classamrex_1_1_distribution_mapping.html#ad8e078b643635f0027eb797c2d54d3b8a6ad808286f21d2e4851a30703a30d063',1,'amrex::DistributionMapping::UNDEFINED()'],['../d4/d03/namespaceamrex.html#abd2e3d292645433cefc947725585f87aa5e543256c480ac577d30f76f9120eb74',1,'amrex::undefined()']]],
  ['undefined_5fv1',['Undefined_v1',['../d2/d02/structamrex_1_1_vis_m_f_1_1_header.html#aa4469030a55d4e3be552189f7d06b54da3394365084045ae347fad00a360767d8',1,'amrex::VisMF::Header']]],
  ['underdetermined',['UNDERDETERMINED',['../dd/dde/class_constrained_l_s.html#a6e412b9d41a6cfeb6b166abf4995491fa7603daf452c6d7075030cf61587fa3cc',1,'ConstrainedLS']]],
  ['unfillable',['Unfillable',['../d4/d03/namespaceamrex.html#a9f13be15ca764e94680df68d8b9505d6a86e193c43ac00a14b0943baf3433c6d0',1,'amrex']]],
  ['ungrownbox',['UngrownBox',['../d7/d50/classamrex_1_1_fill_patch_iterator.html#a8eed2a9ad0b6ac1d61fac0413abbc9c5',1,'amrex::FillPatchIterator']]],
  ['unifyvector',['unifyVector',['../dd/d35/classamrex_1_1_poly_geom.html#a18b0b9bce09fc4cd67518b575a2220e5',1,'amrex::PolyGeom']]],
  ['unionif',['UnionIF',['../df/d06/classamrex_1_1_union_i_f.html',1,'amrex::UnionIF'],['../df/d06/classamrex_1_1_union_i_f.html#a2bab78acccbee9d18661c3504e908c4f',1,'amrex::UnionIF::UnionIF(const Vector&lt; BaseIF *&gt; &amp;a_impFuncs)'],['../df/d06/classamrex_1_1_union_i_f.html#abd06019c5d2cea34d175adaaccc0411f',1,'amrex::UnionIF::UnionIF()'],['../df/d06/classamrex_1_1_union_i_f.html#a9b6c06c59176baf616dba0682d2f1c87',1,'amrex::UnionIF::UnionIF(const UnionIF &amp;a_inputIF)']]],
  ['uniqify',['uniqify',['../d0/dc1/classamrex_1_1_box_array.html#ae3fba31af025b800aef97da09bc4113c',1,'amrex::BoxArray']]],
  ['uniquerandomsubset',['UniqueRandomSubset',['../d4/d03/namespaceamrex.html#aa2c1209819cd8dd79499c2b9f088120e',1,'amrex']]],
  ['uniquestring',['UniqueString',['../d4/d03/namespaceamrex.html#aebee05e08302fb03e29f4270e933239c',1,'amrex']]],
  ['unit',['Unit',['../d9/d02/classamrex_1_1_int_vect.html#a93df7e94c6d2d07ea3eb7647500bf751',1,'amrex::IntVect::Unit()'],['../d9/d6b/classamrex_1_1_real_vect.html#a6745f136d89bdb15e57c3387ca01e973',1,'amrex::RealVect::Unit()'],['../d1/dc1/class_index_t_m.html#a00d4cc95e6e0cba7d32c5106700f8bc3',1,'IndexTM::Unit()']]],
  ['unlikely',['unlikely',['../d5/d93/_a_m_re_x___p_x_stuff_8_h.html#ad8700448546b3b5111404cc021061fd5',1,'AMReX_PXStuff.H']]],
  ['unlinkfile',['UnlinkFile',['../d4/d03/namespaceamrex.html#a8cc0b90b2ed3e37910db70d81484fee8',1,'amrex']]],
  ['unnamed',['Unnamed',['../dc/d36/namespaceamrex_1_1_parallel_descriptor.html#ac6df74b4b37bb6ee58dffea68507d259',1,'amrex::ParallelDescriptor']]],
  ['unprotectednextid',['UnprotectedNextID',['../d4/d53/structamrex_1_1_particle.html#a008787d04da5113188a90a704f8f066a',1,'amrex::Particle']]],
  ['unreadmessages',['unreadMessages',['../d2/d9b/classamrex_1_1_n_files_iter.html#a8ac006de5caf3cfd0e6456dd6c3b3f2a',1,'amrex::NFilesIter']]],
  ['unserializebox',['UnSerializeBox',['../d4/d03/namespaceamrex.html#ad89ecad0ab53b395ee7d2327ab2202c8',1,'amrex']]],
  ['unserializeboxarray',['UnSerializeBoxArray',['../d4/d03/namespaceamrex.html#aca59db20007ff011805104bfc0d235ea',1,'amrex']]],
  ['unserializestringarray',['UnSerializeStringArray',['../d4/d03/namespaceamrex.html#a23045f55caabc0bec388518c4c4adecb',1,'amrex']]],
  ['unset',['unset',['../d8/dc4/classamrex_1_1_index_type.html#a684b7ce727f49817994e1165fa0b49c8',1,'amrex::IndexType']]],
  ['unused_5ftable_5fentries_5fq',['unused_table_entries_q',['../d4/d03/namespaceamrex.html#aebd29bf8d5456e97aae77603a41a032f',1,'amrex']]],
  ['update_5ffab_5fstats',['update_fab_stats',['../d4/d03/namespaceamrex.html#a2d23320cbd486726af914b5bdb5b6d69',1,'amrex::update_fab_stats(long n, long s, size_t szt)'],['../d4/d03/namespaceamrex.html#a2f4b74c08c29c8a404ca762145408f1c',1,'amrex::update_fab_stats(long n, long s, std::size_t szt)']]],
  ['updatebdkey',['updateBDKey',['../df/d93/classamrex_1_1_fab_array_base.html#a3918d10004ca8f4494fd9e6099bdabc3',1,'amrex::FabArrayBase']]],
  ['updatebndryvalues',['updateBndryValues',['../d7/d69/classamrex_1_1_interp_bndry_data.html#a6801628a380e0cec5714dd9e5fe8621b',1,'amrex::InterpBndryData::updateBndryValues(BndryRegister &amp;crse, int c_start, int bnd_start, int num_comp, const IntVect &amp;ratio, int max_order=IBD_max_order_DEF)'],['../d7/d69/classamrex_1_1_interp_bndry_data.html#af1f588505ed7575ed35fcfddfaeb5bfc',1,'amrex::InterpBndryData::updateBndryValues(BndryRegister &amp;crse, int c_start, int bnd_start, int num_comp, int ratio, int max_order=IBD_max_order_DEF)']]],
  ['updatedistributionmaps',['UpdateDistributionMaps',['../d2/d78/classamrex_1_1_amr_level.html#a69fdf4e347142373696281d9269c6799',1,'amrex::AmrLevel']]],
  ['updateneighbors',['updateNeighbors',['../d6/d0b/classamrex_1_1_neighbor_particle_container.html#a274184b09d00fe3f83676b4cb1fa9900',1,'amrex::NeighborParticleContainer']]],
  ['upper_5fbound',['UPPER_BOUND',['../dd/dde/class_constrained_l_s.html#a96327cdbef77a743c8b0554ca6a4ea7ca1549279f88a997f12f6996862586cbe0',1,'ConstrainedLS']]],
  ['upperindex',['UpperIndex',['../d9/de4/classamrex_1_1_coord_sys.html#a348077571bf33062d5119a43cdd0f649',1,'amrex::CoordSys']]],
  ['use_5fefficient_5fregrid',['use_efficient_regrid',['../d4/d03/namespaceamrex.html#af1ff4a567f8d0cdc485062507aac9742',1,'amrex']]],
  ['use_5ffixed_5fcoarse_5fgrids',['use_fixed_coarse_grids',['../d4/d4f/classamrex_1_1_amr_mesh.html#a3162b5c3697f4ce32d585ca434ac40a5',1,'amrex::AmrMesh']]],
  ['use_5ffixed_5fupto_5flevel',['use_fixed_upto_level',['../d4/d4f/classamrex_1_1_amr_mesh.html#a15a5406d39363635d73cc1d986546fe7',1,'amrex::AmrMesh']]],
  ['use_5flist_5fnode_5fallocator',['USE_LIST_NODE_ALLOCATOR',['../d2/d09/_a_m_re_x___k_d_tree_8_h.html#ac0259e59073baaf1f3ad74ca8a2a3a07',1,'AMReX_KDTree.H']]],
  ['useaverage',['UseAverage',['../d3/db7/classamrex_1_1_error_rec.html#adeca3cab9dffebf73c48525e5f94872da7907e45520d436a47eb139e8c055c203',1,'amrex::ErrorRec']]],
  ['usedynamicsetselection',['useDynamicSetSelection',['../da/d45/classamrex_1_1_vis_m_f.html#a9149ec039a2136083c03aabdaa275672',1,'amrex::VisMF']]],
  ['usefixedcoarsegrids',['useFixedCoarseGrids',['../d4/d4f/classamrex_1_1_amr_mesh.html#a2a464ace3aaa3379828d2c12d2b784c4',1,'amrex::AmrMesh']]],
  ['usefixeduptolevel',['useFixedUpToLevel',['../d4/d4f/classamrex_1_1_amr_mesh.html#aa683e0a1c71c53f4f5ccfd9bbdf94cd1',1,'amrex::AmrMesh']]],
  ['usepersistentifstreams',['usePersistentIFStreams',['../da/d45/classamrex_1_1_vis_m_f.html#a806512ed6337ff31d9eb6f63f94ce792',1,'amrex::VisMF']]],
  ['useprepost',['usePrePost',['../dd/db6/classamrex_1_1_particle_container.html#acb1ee952f82bdf31959c8d72cbfb454b',1,'amrex::ParticleContainer']]],
  ['usesingleread',['useSingleRead',['../da/d45/classamrex_1_1_vis_m_f.html#a7127d8495966b5d5506d3cff6c536a12',1,'amrex::VisMF']]],
  ['usesinglewrite',['useSingleWrite',['../da/d45/classamrex_1_1_vis_m_f.html#a0706ea7e89d0d5b2d9a60acacc8f38f0',1,'amrex::VisMF']]],
  ['usesparsefpp',['useSparseFPP',['../d2/d9b/classamrex_1_1_n_files_iter.html#a97ff46c50d110eeeacfcb90f5c001bd0',1,'amrex::NFilesIter']]],
  ['usestaticsetselection',['useStaticSetSelection',['../d2/d9b/classamrex_1_1_n_files_iter.html#ab85302b89ef7c47f7213adc1c25e7fbe',1,'amrex::NFilesIter']]],
  ['usesynchronousreads',['useSynchronousReads',['../da/d45/classamrex_1_1_vis_m_f.html#a0c9e87f4c459196a9f1ce7870ccbf9cb',1,'amrex::VisMF']]],
  ['using_5ftop_5fface_5fmoments',['USING_TOP_FACE_MOMENTS',['../d6/d2e/_a_m_re_x___notation_8_h.html#a862e4fe418b80fd28688a82cc96e6b08',1,'AMReX_Notation.H']]],
  ['usingprecreatedirectories',['UsingPrecreateDirectories',['../d0/d2e/classamrex_1_1_amr.html#aab1d9e9bbbff4d4bf9b3b57896574364',1,'amrex::Amr']]],
  ['usleep',['USleep',['../d4/d03/namespaceamrex.html#af353083e449d736188c80257c096f518',1,'amrex']]],
  ['utilcreatecleandirectory',['UtilCreateCleanDirectory',['../d4/d03/namespaceamrex.html#a832f4ad883cbc470d76cde70a099e912',1,'amrex']]],
  ['utilcreatedirectory',['UtilCreateDirectory',['../d4/d03/namespaceamrex.html#a913fd7ccf7cab45bc7d8e2a09ddd2824',1,'amrex']]],
  ['utilcreatedirectorydestructive',['UtilCreateDirectoryDestructive',['../d4/d03/namespaceamrex.html#aebce007d4152715af0cf44f3ab94d1fe',1,'amrex']]],
  ['utilrenamedirectorytoold',['UtilRenameDirectoryToOld',['../d4/d03/namespaceamrex.html#ae2d0c6a2a5153df0d38d7cde07a89c73',1,'amrex']]]
];
