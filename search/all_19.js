var searchData=
[
  ['y2der',['Y2DER',['../d7/d48/_a_m_re_x___i_n_t_e_r_p_b_n_d_r_y_d_a_t_a__3_d_8_f90.html#a426aa726ff15d4389d4fa4ef9d4b5510',1,'AMReX_INTERPBNDRYDATA_3D.F90']]],
  ['yafluxregister',['YAFluxRegister',['../da/d7a/classamrex_1_1_y_a_flux_register.html',1,'amrex::YAFluxRegister'],['../da/d7a/classamrex_1_1_y_a_flux_register.html#abfac49b8daf5d0b0303aaa24a4f7751a',1,'amrex::YAFluxRegister::YAFluxRegister()=default'],['../da/d7a/classamrex_1_1_y_a_flux_register.html#a4cb85ba705d31548325bd3f09c6f17ff',1,'amrex::YAFluxRegister::YAFluxRegister(const BoxArray &amp;fba, const BoxArray &amp;cba, const DistributionMapping &amp;fdm, const DistributionMapping &amp;cdm, const Geometry &amp;fgeom, const Geometry &amp;cgeom, const IntVect &amp;ref_ratio, int fine_lev, int nvar)']]],
  ['yder',['YDER',['../d7/d48/_a_m_re_x___i_n_t_e_r_p_b_n_d_r_y_d_a_t_a__3_d_8_f90.html#ab9e4b79501cc47488b6532a963bd9ca2',1,'AMReX_INTERPBNDRYDATA_3D.F90']]]
];
