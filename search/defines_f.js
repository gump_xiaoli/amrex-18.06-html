var searchData=
[
  ['recursive_5fgeometry_5fgeneration',['RECURSIVE_GEOMETRY_GENERATION',['../d6/d2e/_a_m_re_x___notation_8_h.html#a5ffb36d9ee9f8d669ec211f20bb545f3',1,'AMReX_Notation.H']]],
  ['refinement_5fin_5fzdirection',['REFINEMENT_IN_ZDIRECTION',['../d6/d2e/_a_m_re_x___notation_8_h.html#a072eaaf6b6eebbee3ced2dd1add539c0',1,'AMReX_Notation.H']]],
  ['refinement_5fwith_5fresiduals',['REFINEMENT_WITH_RESIDUALS',['../d6/d2e/_a_m_re_x___notation_8_h.html#aef140506364c1b1dbd6e62cc858fab43',1,'AMReX_Notation.H']]],
  ['refinementthreshold',['REFINEMENTTHRESHOLD',['../d6/d2e/_a_m_re_x___notation_8_h.html#a292db339b0816c04577ab7c4bb16d496',1,'AMReX_Notation.H']]],
  ['reflect_5feven',['REFLECT_EVEN',['../d2/d84/_a_m_re_x___b_c___t_y_p_e_s_8_h.html#a3084fca6e965f4bac15cee757a39915e',1,'AMReX_BC_TYPES.H']]],
  ['reflect_5fodd',['REFLECT_ODD',['../d2/d84/_a_m_re_x___b_c___t_y_p_e_s_8_h.html#af3aeaaa2c4f177482545bae75d04656f',1,'AMReX_BC_TYPES.H']]],
  ['restrict',['RESTRICT',['../d2/d09/_a_m_re_x___k_d_tree_8_h.html#aae3356b63849abbe8789dd41648ee90a',1,'AMReX_KDTree.H']]]
];
